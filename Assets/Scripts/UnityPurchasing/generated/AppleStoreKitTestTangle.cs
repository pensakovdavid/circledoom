// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleStoreKitTestTangle
    {
        private static byte[] data = System.Convert.FromBase64String("raOxsU+0tYCzsbFPgL62s+Wtv7HXlQkb3IPVHWhyKsj4v1l8cZe9dvEVzrk9Co2NfcDnaF1fZWrHeCO0u4n8e//ecAPvByUKCRjt+sQbX9/4Nke9sbmxprjjxN/C1fvZxIAysQ1wZq3wLucJiCDHxuJiGPvA3gjXgDKzxIAysuwQs7KxsrKxsYC9trk8UcFr+vVDnakms+YxFvRexcfr42dUyBgfGN0XcWrYndZ8f5m74JxTkoC9trmaNvg2R72xsbG1sLMysb8pSwM65S9frZgI75phwy+F94cL9nD3mPtb6nGbueacFRq5s8uCgWxYR72xsbu1sLMysbGwArBQjEFYWdLTGycqm3OaaGACUWXqvYmr39eYGYWCg4Xqp72EgICDgoeBh4WCg4XqgLq2uJu2sbW1t7OzgL22uZo2+DZ/1IzdMOmDPHqGtpqhqz35frhP1uPE38LV+9nEgK6nvYKAgISAgYGHgaGAv7az5bS7vLjjxN/C1fvZxIE7HEUvALmIE97Le5bguqpROWHCxnQZuwN/0r9LBAxHazTumFv3vUFd+yMLukgRkY540WmUeO6vxY5qI2uxT7S0s7KyNICmtrPlrZWxsU+0vHJLVu+icbKzsbCxE4uAiYC/trPlgaohMAkPVbx7jAu+9NKWSrSZBvk1yPh+cHWiodq/vB6ftXrfytTPkL0LiCDbtnRMvVUl4H0heAqVROh5nMVW/AGiY01Tr+69zQjoZxDMVM+0s7y448TfwtX72cSBoYC/trPltH/IHO0Kju/gm+yZPG1yZ5qnb5IEV99Qq1a92H2V3nPGuEX6ux7KxIy7gLm2s+W0tqOy5eOBp4CltrmaNrq8uOPE38LV+9nEgaGAv7az5bS7Y8agQkDZXH3SKcFJ0Do0+Ih0DTvEgaGAv7az5bS6vLjjxN/C1fvZxNDGh+JcTxyp7bylnzq/RTl4Z5utsuXjgaeApba5mjb4Nke9sbmxpriwgDKxurIysbGwa88gjJzDv2snlpo2+DZHvbGxu7WwgO+BoYC/trPlvLjjxN/C1fvZxIG7gLm2s+W0tqOA74GhgL+2s+W0s7y448TfwtX72VsH8DGNN43A4WF07T0VbM/a6KpuojNmN8jdKQ3uA7pQKH5GWnM+3dmsX7k4yNV+MUI9wle3qt6WokeCAa5cmyG1F43/");
        private static int[] order = new int[] { 28,35,21,20,12,32,28,24,26,13,33,37,31,40,24,30,36,28,33,19,33,30,29,27,36,38,27,39,30,38,30,37,33,34,40,43,40,40,43,40,40,42,42,43,44 };
        private static int key = 176;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
