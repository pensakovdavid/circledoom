// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("XVsw9aajklrQFS6iJ/exHrQb6EES2P3RPfBeidOPCmHlaLxkdbnv8tkIlPGAW1AKLMzpLNyiQaaQp1gWwqUB6sAQpQTFga9EG6SdXeI3CMpvhxoyqEb5AV67/cDPCEwZ2DPt/p9kF6LdGj7owVIG3kczSQZfr5JcV8FuMzB/DC6CTHfJEyQafnJWgb9d2jJQRQQqtK6Hs5dMkbIunrxbBxS0lIuXPChRaA41IMqrRyUbeq5rjA8BDj6MDwQMjA8PDqhE5CQOL1x/UARt5j6dVdkDd47t1WqubeO+89bZ8rOp3KQPdB7BaO/weUlSgMPNS68ued/lYwQUXBHPYvdQVtxyoQI+jA8sPgMIBySIRoj5Aw8PDwsODSQGveexxywqzwwNDw4P");
        private static int[] order = new int[] { 4,13,12,10,5,12,11,10,10,13,10,13,13,13,14 };
        private static int key = 14;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
