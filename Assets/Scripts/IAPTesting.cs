using GameName.Scripts;
using GameName.Scripts.Game.Controllers;
using GameName.Scripts.Game.SavePlayerData;
using UnityEngine;
using UnityEngine.Purchasing;

namespace GameName.CompleteProject {
    public class IAPTesting : MonoBehaviour, IStoreListener {
        private static IStoreController m_StoreController;

        [SerializeField] private TMPro.TextMeshProUGUI priceText;
        [SerializeField] private GameObject btnNoAds;
        [SerializeField] private string coinsID;
        [SerializeField] private string adsID;

        void Start() {
            InitializePurchasing();
            RestoreVariable();
        }

        void InitializePurchasing() {
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            builder.AddProduct(coinsID, ProductType.Consumable);
            builder.AddProduct(adsID, ProductType.NonConsumable);

            UnityPurchasing.Initialize(this, builder);
        }

        void RestoreVariable() {
            if (PlayerPrefs.GetInt("ads") == 1) {
                btnNoAds.SetActive(false);
            }
        }

        public void BuyProduct(string productName) => m_StoreController.InitiatePurchase(productName);

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) {
            var product = args.purchasedProduct;

            if (product.definition.id == adsID) {
                ProductNoAds();
            }

            if (product.definition.id == coinsID) {
                ProductCoins();
            }

            Debug.Log($"Purchase Complete - Product: {product.definition.id}");

            return PurchaseProcessingResult.Complete;
        }

        private void ProductCoins() {
            MoneyController.Instance.AddMoney(500);
            PlayerAccount.Instance.SaveChanges();
        }

        private void ProductNoAds() {
            PlayerPrefs.SetInt("ads", 1);
            btnNoAds.SetActive(false);
            AdsInvoker.Instance.DisablingADs();
        }

        public void OnInitializeFailed(InitializationFailureReason error) {
            Debug.Log($"In-App Purchasing initialize failed: {error}");
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason) {
            Debug.Log($"Purchase failed - Product: '{product.definition.id}', PurchaseFailureReason: {failureReason}");
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
            Debug.Log("In-App Purchasing successfully initialized");
            m_StoreController = controller;
        }

        public void UpdateText(string productId) {
            if (m_StoreController.products.WithID(productId) != null) {
                if (priceText != null) {
                    priceText.text = $"Buy \'{m_StoreController.products.WithID(productId).metadata.localizedDescription}\' for " +
                        $"{m_StoreController.products.WithID(productId).metadata.localizedPrice} {m_StoreController.products.WithID(productId).metadata.isoCurrencyCode}";
                }
            }
        }
    }
}