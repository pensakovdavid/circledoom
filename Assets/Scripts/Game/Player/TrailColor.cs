using GameName.Scripts.Game.Controllers;
using UnityEngine;

namespace GameName.Scripts.Game.Player {    
    public class TrailColor : MonoBehaviour {
        [SerializeField] private TrailRenderer trailRenderer;

        void Start() {
            trailRenderer.startColor = ColorController.Instance.Color.PlayerColor - new Color(0, 0, 0, .2f);
            trailRenderer.endColor = ColorController.Instance.Color.BGColor;
        }
    }
}