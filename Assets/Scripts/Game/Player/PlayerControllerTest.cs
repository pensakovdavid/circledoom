using GameName.Scripts.Game.Controllers;
using GameName.Scripts.Game.Enemy;
using UnityEngine;

namespace GameName.Scripts.Game.Player {
    public class PlayerControllerTest : MonoBehaviour {
        [SerializeField] private SpriteRenderer spriteRenderers;
        [SerializeField] private GameObject brokenCircle;
        [SerializeField] private GameObject player;
        [SerializeField] private float _speedRotation;

        private float _speedScale;
        
        private Vector3 _roundDirection;
        private bool _roundClockwise;

        private float CurentSpeed => Time.deltaTime * (_speedRotation * _speedScale);

        private void Start() {
            EventsController.Instance.OnEndGame += EndGame;
            EventsController.Instance.OnTab += OnPointerTab;
            EventsController.Instance.OnProgressBarFill += SpeedScale;
            Initialization();
        }

        private void Update() => Move();

        void Initialization() {
            _speedScale = 1;
            _roundClockwise = true;
            _roundDirection = Vector3.forward;
        }

        void Move() {
            transform.RotateAround(transform.position, _roundDirection, Mathf.SmoothStep(0, 90, CurentSpeed));
        }

        void EndGame() {
            if (player != null)
                player.SetActive(false);
            if (brokenCircle != null)
                brokenCircle.SetActive(true);

            EventsController.Instance.OnProgressBarFill -= SpeedScale;
            EventsController.Instance.OnTab -= OnPointerTab;
            EventsController.Instance.OnEndGame -= EndGame;
        }

        public void OnPointerTab() {
            if (_roundClockwise) {
                _roundDirection = Vector3.forward;
                _roundClockwise = false;
                spriteRenderers.flipX = false;
            } else if (!_roundClockwise) {
                _roundDirection = Vector3.back;
                _roundClockwise = true;
                spriteRenderers.flipX = true;
            }
        }

        void SpeedScale(int i) { if (i == 1) { _speedScale += 0.05f; } }

        void OnTriggerEnter2D(Collider2D collision) {
            IObstacle obstacle = collision.GetComponent<IObstacle>();
            if (obstacle != null) {
                EventsController.Instance.OnEndGameInvoke();
            }
        }
    }
}