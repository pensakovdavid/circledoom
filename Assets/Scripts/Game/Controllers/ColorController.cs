using GameName.Scripts.Game.Colors;
using GameName.Scripts.Game.SavePlayerData;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameName.Scripts.Game.Controllers {
    public class ColorController : MonoBehaviour {
        private static ColorController _instance;
        public static ColorController Instance {
            get {
                if (_instance == null) {
                    GameObject obj = new GameObject("ColorController", typeof(ColorController));
                    _instance = obj.GetComponent<ColorController>();
                    DontDestroyOnLoad(obj);
                }
                return _instance;
            }
        }

        private ColorsSO _colors;
        private ColorElement _color;
        private GameColorMaterial _colorMaterial;
        private int _curentColor;

        #region Events
        public event Action<int> OnChangedColor;
        #endregion

        public ColorsSO Colors => _colors;
        public int CurentColor => _curentColor;
        public ColorElement Color => _color;

        public void Initialize(ColorsSO colorsSO, GameColorMaterial gameColorMaterial) {
            _colors = colorsSO;
            _colorMaterial = gameColorMaterial;

            _curentColor = PlayerAccount.Instance.Data.CurentColor;

            _color = _colors.Colors[_curentColor];

            SetColor();
        }

        public void ChangeColor(int key) {
            _curentColor = key;
            _color = _colors.Colors[_curentColor];
            
            SetColor();

            PlayerAccount.Instance.SaveChanges();           
            SceneManager.LoadScene(1);
        }

        void SetColor() {
            _colorMaterial.EnemyColor.color = _color.EnemyColor;
            _colorMaterial.PlayerColor.color  = _color.PlayerColor;
            _colorMaterial.BGColor.color = _color.BGColor;
            _colorMaterial.ParticleColor.color = _color.ParticleColor;
            _colorMaterial.ProgressBarColor1.color = _color.ProgressBarColor1;
            _colorMaterial.ProgressBarColor2.color = _color.ProgressBarColor2;

            _colorMaterial.EnemyObstacleColor.color = _color.EnemyColor * 1.2f;
            _colorMaterial.LoadingGameBarColor.color = _color.EnemyColor;            
        }
    }
}