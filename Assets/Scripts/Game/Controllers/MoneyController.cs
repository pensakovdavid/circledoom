﻿using GameName.Scripts.Game.SavePlayerData;
using System;
using UnityEngine;

namespace GameName.Scripts.Game.Controllers {
    internal class MoneyController : MonoBehaviour {
        private static MoneyController _instance;
        public static MoneyController Instance {
            get {
                if (_instance == null) {
                    GameObject obj = new GameObject("MoneyController", typeof(MoneyController));
                    _instance = obj.GetComponent<MoneyController>();
                    _instance.Initialize();
                    DontDestroyOnLoad(obj);
                }
                return _instance;
            }
        }

        #region Event
        public event Action OnBoostMoney;
        #endregion

        private void Initialize() => _money = PlayerAccount.Instance.Data.Money;

        public int Money => _money;

        private int _money;

        public bool MakePurchase(int price) {
            if(price <= Money) {
                _money -= price;
                return true;
            }
            return false;
        }
        public void AddMoney(int i) => MoneyUp(i);

        private void MoneyUp(int i) {
            _money += i;
            OnBoostMoney?.Invoke();
        }
    }
}