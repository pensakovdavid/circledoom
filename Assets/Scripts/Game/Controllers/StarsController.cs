﻿using GameName.Scripts.Game.SavePlayerData;
using System.Collections.Generic;
using UnityEngine;

namespace GameName.Scripts.Game.Controllers {
    public class StarsController : MonoBehaviour {
        private static StarsController _instance;
        public static StarsController Instance {
            get {
                if (_instance == null) {
                    GameObject obj = new GameObject("StarsController ", typeof(StarsController));
                    _instance = obj.GetComponent<StarsController>();
                    _instance.Initialize();
                    DontDestroyOnLoad(obj);
                }
                return _instance;
            }
        }

        #region fields
        private int _quantityScoreStar1;
        private int _quantityScoreStar2;
        private int _quantityScoreStar3;
        private int _quantityStars;
        private int _curentScore;
        private int _lvl;

        private bool _rewardForStars1 = false;
        private bool _rewardForStars2 = false;
        private bool _rewardForStars3 = false;
        #endregion

        private Dictionary<int, StarsBtnData> _stars;
        public Dictionary<int, StarsBtnData> BtnStarsData => _stars;

        private void Initialize() {
            ScoreController.Instance.OnBoostScore += BoostScore;
            _stars = PlayerAccount.Instance.Data.Stars;            
        }

        private void BoostScore(int obj) => UpdateStarsBtnData();

        void UpdateStarsBtnData() {
            _lvl = SceneNavigator.GetCurentScene();
            _curentScore = ScoreController.Instance.BestScoreLvl[_lvl];

            RevardSars(_lvl);

            _quantityScoreStar1 = 50 + 35 * _lvl;
            _quantityScoreStar2 = 100 + 75 * _lvl;
            _quantityScoreStar3 = 200 + 170 * _lvl;

            _quantityStars = 0;
            if (_curentScore >= _quantityScoreStar1) {
                _quantityStars = 1;
                if (!_rewardForStars1) {
                    MoneyController.Instance.AddMoney(50);
                    _rewardForStars1 = true;
                }
            }
            if (_curentScore >= _quantityScoreStar2) { 
                _quantityStars = 2;
                if (!_rewardForStars2) {
                    MoneyController.Instance.AddMoney(100);
                    _rewardForStars2 = true;
                }
            }
            if (_curentScore >= _quantityScoreStar3) {
                _quantityStars = 3;
                if (!_rewardForStars3) {
                    MoneyController.Instance.AddMoney(200);
                    _rewardForStars3 = true;
                }
            }

            _stars[_lvl].QuantityStars = _quantityStars;
        }

        void RevardSars(int i) {
            _rewardForStars1 = false;
            _rewardForStars2 = false;
            _rewardForStars3 = false;


            if (_stars[i].QuantityStars >= 1)
                _rewardForStars1 = true;
            if (_stars[i].QuantityStars >= 2)
                _rewardForStars2 = true;
            if (_stars[i].QuantityStars >= 3)
                _rewardForStars3 = true;
        }

        public int GetTotalStars() {
            int totalStars = 0;
            for (int i = 1; i < _stars.Count; i++) {
                totalStars += _stars[i].QuantityStars;
            }
            return totalStars;
        }
    }
}