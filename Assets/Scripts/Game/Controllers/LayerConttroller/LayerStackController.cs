﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameName.Sripts.Game.Controllers.LayerConttroller {
    public class LayerStackController : MonoBehaviour{
        private static LayerStackController _instance;
        public static LayerStackController Instance {
            get {
                if (_instance == null) {
                    GameObject obj = new GameObject("LayerController", typeof(LayerStackController));
                    _instance = obj.GetComponent<LayerStackController>();
                    _instance.Initialize();
                    DontDestroyOnLoad(obj);
                }
                return _instance; 
            }
        }

        private Canvas _canvas;
        private Queue<ILayer> _layers;

        void Initialize() {
        }

        public void Invoke(ILayer layer) { 
            
        }
    }
}