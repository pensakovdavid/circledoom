﻿using GameName.Scripts.Game.SavePlayerData;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameName.Scripts.Game.Controllers {
    internal class ScoreController : MonoBehaviour {
        private static ScoreController _instance;
        public static ScoreController Instance {
            get {
                if (_instance == null) {
                    GameObject obj = new GameObject("ScoreController", typeof(ScoreController));
                    _instance = obj.GetComponent<ScoreController>();
                    _instance.Initialize();
                    DontDestroyOnLoad(obj);
                }
                return _instance;
            }
        }
        private void Initialize() {
            EventsController.Instance.OnProgressBarFill += ScoreScaleUp;
            EventsController.Instance.OnEndGame += EndGame;

            _bestScoreLvl = PlayerAccount.Instance.Data.Results;
            _curentScore = 0;
            _scoreScale = 1;
        }

        #region Event
        public event Action<int> OnBoostScore;
        #endregion

        private int _curentScore;
        private int _scoreScale;
        private Dictionary<int, int> _bestScoreLvl;

        public int Score;
        public int ScoreScale => _scoreScale;
        public Dictionary<int, int> BestScoreLvl => _bestScoreLvl;

        public KeyValuePair<int, int> GetBestRecord() {
            KeyValuePair<int, int> i;
            foreach (var item in _bestScoreLvl)
                if (item.Value > i.Value)
                    i = item;

            return i;
        }

        public void ScoreUp(int i) {
            _curentScore += i * _scoreScale;
            Score = _curentScore;
            UpdateRecordLvl(Score);

            OnBoostScore?.Invoke(i);
        }

        void UpdateRecordLvl(int score) {
            int _lvl = SceneNavigator.GetCurentScene();

            if (_bestScoreLvl.ContainsKey(_lvl)) {
                if (score > _bestScoreLvl[_lvl])
                    _bestScoreLvl[_lvl] = score;
            } else {
                _bestScoreLvl.Add(_lvl, score);
            }
        }

        void ScoreScaleUp(int i) {
            if (i == 1) {
                _scoreScale += 1;
            }
        }

        void EndGame() {
            Score = _curentScore;
            UpdateRecordLvl(Score);    
            _curentScore = 0;
            _scoreScale = 1;
        }
    }
}
