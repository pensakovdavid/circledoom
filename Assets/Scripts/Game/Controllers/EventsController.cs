﻿using System;
using UnityEngine;

namespace GameName.Scripts.Game.Controllers {
    public class EventsController : MonoBehaviour {
        private static EventsController _instance;
        public static EventsController Instance {
            get {
                if (_instance == null) {
                    GameObject obj = new GameObject("EventController", typeof(EventsController));
                    _instance = obj.GetComponent<EventsController>();
                    DontDestroyOnLoad(obj);
                }
                return _instance;
            }
        }

        private event Action _OnEndGame;
        private event Action _OnTab;
        private event Action<int> _OnProgressBarFull;


        public event Action OnEndGame {
            add { _OnEndGame += value; }
            remove { _OnEndGame -= value; }
        }
        public void OnEndGameInvoke() => _OnEndGame?.Invoke();



        public event Action OnTab {
            add { _OnTab += value; }
            remove { _OnTab -= value; }
        }
        public void OnTabInvoke() => _OnTab?.Invoke();



        public event Action<int> OnProgressBarFill {
            add { _OnProgressBarFull += value; }
            remove { _OnProgressBarFull -= value; }
        }
        public void OnProgressBarFullInvoke(int i) => _OnProgressBarFull?.Invoke(i);
    }
}