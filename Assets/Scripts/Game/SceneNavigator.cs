﻿using UnityEngine.SceneManagement;

namespace GameName.Scripts.Game {
    public class SceneNavigator {
        public struct MainScanes {
            public const int LoadingGameScene = 0;
            public const int GameMenuScene = 1;
        }
        public struct GameLvl {
            public static int[] lvls => new int[] {
                0,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
            };
        }
        public static int GetCurentScene() {
            int i = SceneManager.GetActiveScene().buildIndex;
            return i;
        }
        public static void LoadingSceneAsync(int i) => SceneManager.LoadSceneAsync(i);
        public static void ReloadingSceneAsync() => SceneManager.LoadSceneAsync(GetCurentScene());
    }
}