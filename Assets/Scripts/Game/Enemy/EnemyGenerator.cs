using GameName.Scripts.Game.Enums;
using System.Collections.Generic;
using UnityEngine;

namespace GameName.Scripts.Game.Enemy {
    public class EnemyGenerator : MonoBehaviour {
        [Header("Enemy data")]
        [SerializeField] private EnemyData enemysTypePrifab;
        [Header("Spawn data")]
        [SerializeField] private int quantityEnemy;
        [SerializeField] private float[] distanceBetweenEnemies;
        [SerializeField] private float[] areaSpawn;
        [SerializeField] private DirectionMove direction;

        #region private fields
        private int randDistance = 0, randArea = 0;
        private float _startSpawnX = 10;
        private List<GameObject> _enemysPrifab;
        private Vector3 _startPoint;
        #endregion

        public float[] AreaSpawn => areaSpawn;
        public float ExtremePoint => _startSpawnX - 10;
        public EnemyData EnemysData => enemysTypePrifab;

        void Start() {
            _enemysPrifab = new List<GameObject>();
            for (int i = 0; i < quantityEnemy; i++)
                _enemysPrifab.Add(enemysTypePrifab.Object);

            Spawn();
            Rotation();
        }

        void Spawn() {
            for (int i = 0; i < _enemysPrifab.Count; i++) {
                randDistance = Random.Range(0, distanceBetweenEnemies.Length);
                randArea = Random.Range(0, areaSpawn.Length);

                _startPoint = new Vector3(_startSpawnX, areaSpawn[randArea], 0) * -1;
                _startSpawnX += distanceBetweenEnemies[randDistance];

                _enemysPrifab[i].transform.position = _startPoint;
                Instantiate(_enemysPrifab[i]).transform.SetParent(gameObject.transform);
            }
        }

        void Rotation() {
            if (direction == DirectionMove.LeftToRight) {
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            } else if (direction == DirectionMove.RightToLeft) {
                transform.rotation = new Quaternion(0f, 0f, 180f, 0f);
            } else if (direction == DirectionMove.TopToBottom) {
                transform.rotation = new Quaternion(0f, 0f, -90, 90f);
            } else if (direction == DirectionMove.BottomToTop) {
                transform.rotation = new Quaternion(0f, 0f, 90f, 90f);
            }
        }
    }    
}