using UnityEngine;

namespace GameName.Scripts.Game.Enemy {
    public class ObstacleAroundMove : Enemy, IObstacle {
        [SerializeField] private RoundDir roundDir;
        [SerializeField] private float speedRotation = 1;

        private Vector3 _roundDirection;
        private float CurentSpeed => Time.deltaTime * speedRotation;

        private void Start() {
            if (roundDir == RoundDir.Clockwise) {
                _roundDirection = Vector3.forward;
            }
            else if (roundDir == RoundDir.Counterclockwise) {
                _roundDirection = Vector3.back;
            }
        }
        private void Update() {
            Move();    
        }

        public override void Move() {
            transform.RotateAround(transform.position, _roundDirection, Mathf.SmoothStep(0, 90, CurentSpeed));
        }
    }

    enum RoundDir {
        Clockwise,
        Counterclockwise
    }
}