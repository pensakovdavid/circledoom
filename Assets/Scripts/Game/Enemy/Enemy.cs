﻿using UnityEngine;

namespace GameName.Scripts.Game.Enemy {
    public abstract class Enemy : MonoBehaviour {
        public abstract void Move();
    }
}