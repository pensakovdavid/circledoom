using UnityEngine;

namespace GameName.Scripts.Game.Enemy {
    public class ObstaclesMoving : Enemy {
        [SerializeField] private float distance;
        [SerializeField] private float speedMove;

        [SerializeField] private GameObject _obstacle_1;
        [SerializeField] private GameObject _obstacle_2;

        Vector3 startPosition1;
        Vector3 startPosition2;
        Vector3 velocity;

        public override void Move() {
            if (_obstacle_1.transform.localPosition.y > startPosition1.y + distance)
                velocity.y = -speedMove;
            else if (_obstacle_1.transform.localPosition.y < startPosition1.y - distance)
                velocity.y = speedMove;
            _obstacle_1.transform.localPosition += velocity * Time.deltaTime;

            if (_obstacle_2.transform.localPosition.y > startPosition2.y + distance)
                velocity.y = -speedMove;
            else if (_obstacle_2.transform.localPosition.y < startPosition2.y - distance)
                velocity.y = speedMove;

            _obstacle_2.transform.localPosition += velocity * Time.deltaTime;
        }

        void Start() {
            startPosition1 = _obstacle_1.transform.localPosition;
            startPosition2 = _obstacle_2.transform.localPosition;
            velocity = Vector3.up * speedMove;
        }

        void Update() => Move();
    }
}