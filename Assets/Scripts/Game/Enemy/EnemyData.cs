﻿using UnityEngine;
using System;

namespace GameName.Scripts.Game.Enemy {
    [Serializable]
    public class EnemyData {
        [SerializeField] private GameObject _prifab;
        [SerializeField] private float _speed;
        [SerializeField] private int _numberPoints;

        public GameObject Object { get => _prifab; set => _prifab = value; }
        public float Speed { get => _speed; set => _speed = value; }
        public int NumberPoints { get => _numberPoints; set => _numberPoints = value; }
    }
}
