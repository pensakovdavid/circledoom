using GameName.Scripts.Game.Controllers;
using UnityEngine;

namespace GameName.Scripts.Game.Enemy {
    public class OrdinaryEnemy : Enemy, IObstacle {

        #region private fields
        private int _numberPoint;
        private float _enemySpeed;
        private float _speedScale;
        private float _endPosX;
        private float _spawnDistance;
        private float[] _areaSpawn;
        private Transform _transformParent;
        private EnemyGenerator _controller;
        private EnemyData _enemy;
        #endregion

        private Vector3 EndPoint { get => new Vector3(_endPosX, transform.localPosition.y, 1); }
        private float CurentSpeed { get => Time.deltaTime * (_enemySpeed * _speedScale); }
        
        public void Start() {
            Initialization();

            _enemy = _controller.EnemysData;
            _enemySpeed = _enemy.Speed;
            _numberPoint = _enemy.NumberPoints;
            _speedScale = 1;
        }

        public void Update() => Move();

        void Initialization() {
            EventsController.Instance.OnProgressBarFill += SpeedScaleUp;

            _controller = GetComponentInParent<EnemyGenerator>();
            _transformParent = GetComponentInParent<Transform>();

            _spawnDistance = _controller.ExtremePoint;
            _areaSpawn = _controller.AreaSpawn;
            _endPosX = _transformParent.position.x + _spawnDistance;
        }

        public override void Move() {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, EndPoint, CurentSpeed);
            if (transform.localPosition.x >= 15f) {
                ScoreController.Instance.ScoreUp(_numberPoint);
                Spawn();
            }
        }

        void Spawn() {
            float y = _areaSpawn[Random.Range(0, _areaSpawn.Length)];
            transform.localPosition = new Vector3(transform.localPosition.x - _spawnDistance, y, 0);
        }

        void SpeedScaleUp(int i) { if (i == 0) { _speedScale += 0.05f; } }
    }
}