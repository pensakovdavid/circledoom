﻿using UnityEngine;

namespace GameName.Scripts.Game.Colors {
    internal class ColorParticleObj : ColorObj {
        [SerializeField] private ParticleSystem particle;
       
        void Start() {
            var a = particle.main;
            a.startColor = SetColor();
        }
    }
}
