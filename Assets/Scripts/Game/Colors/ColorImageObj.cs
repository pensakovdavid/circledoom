﻿using UnityEngine;
using UnityEngine.UI;

namespace GameName.Scripts.Game.Colors {
    internal class ColorImageObj : ColorObj {
        [SerializeField] private Image image;
        void Start() => image.color = SetColor();       
    }
}
