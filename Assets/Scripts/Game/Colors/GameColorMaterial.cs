﻿using UnityEngine;

namespace GameName.Scripts.Game.Colors {

    [CreateAssetMenu(fileName = "GameColorMaterials", menuName = "SettingsData/GamesColorMaterials", order = 1)]
    public class GameColorMaterial : ScriptableObject{
        [SerializeField] private Material bgColor;
        [SerializeField] private Material playerColor;
        [SerializeField] private Material enemyColor;
        [SerializeField] private Material particleColor;
        [SerializeField] private Material progressBarColor1;
        [SerializeField] private Material progressBarColor2;

        [SerializeField] private Material enemyObstacleColor;
        [SerializeField] private Material loadingGameBarColor;


        public Material BGColor { get => bgColor; }
        public Material PlayerColor { get => playerColor; }
        public Material EnemyColor { get => enemyColor; }
        public Material ParticleColor { get => particleColor; }
        public Material ProgressBarColor1 { get => progressBarColor1; }
        public Material ProgressBarColor2 { get => progressBarColor2; }

        public Material EnemyObstacleColor { get => enemyObstacleColor; }
        public Material LoadingGameBarColor { get => loadingGameBarColor; }
    }
}
