﻿using GameName.Scripts.Game.Controllers;
using GameName.Scripts.Game.Enums;
using UnityEngine;

namespace GameName.Scripts.Game.Colors {
    internal abstract class ColorObj : MonoBehaviour {
        [SerializeField] private ColorType colorType;
        [SerializeField] private float tinting = 1f;
        [SerializeField] private float alpha = 1f;
        [SerializeField] private Color arbitraryColor;

        public Color SetColor() {
            Color color = Color.white;
            switch (colorType) {
                case ColorType.PlayerColor:
                    color = ColorController.Instance.Color.PlayerColor;
                    break;
                case ColorType.EnemyColor:
                    color = ColorController.Instance.Color.EnemyColor;
                    break;
                case ColorType.ParticleColor:
                    color = ColorController.Instance.Color.ParticleColor;
                    break;
                case ColorType.BGColor:
                    color = ColorController.Instance.Color.BGColor;
                    break;
                case ColorType.ProgressBarColor1:
                    color = ColorController.Instance.Color.ProgressBarColor1;
                    break;
                case ColorType.ProgressBarColor2:
                    color = ColorController.Instance.Color.ProgressBarColor2;
                    break;
                case ColorType.Arbitrary:
                    color = arbitraryColor;
                    break;
            }
            color = new Color(color.r * tinting, color.g * tinting, color.b * tinting, alpha);

            return color;
        }
    }
}
