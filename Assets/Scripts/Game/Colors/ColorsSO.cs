﻿using UnityEngine;

namespace GameName.Scripts.Game.Colors {
    [CreateAssetMenu(fileName = "Colors", menuName = "SettingsData/Colors", order = 1)]
    public class ColorsSO : ScriptableObject{
        [SerializeField] private ColorElement[] _colors;
        public ColorElement[] Colors => _colors;
    }
}
