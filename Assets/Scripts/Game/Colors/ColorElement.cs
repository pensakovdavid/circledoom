﻿using UnityEngine;

namespace GameName.Scripts.Game.Colors {
    [System.Serializable] [CreateAssetMenu(fileName = "ColorsElement", menuName = "SettingsData/ColorsElement", order = 1)]
    public class ColorElement : ScriptableObject {
        [SerializeField] private bool purchased = false;
        [SerializeField] private int price = 100;
        [SerializeField] private Color bgColor;
        [SerializeField] private Color playerColor;
        [SerializeField] private Color enemyColor;
        [SerializeField] private Color particleColor;
        [SerializeField] private Color progressBar1;
        [SerializeField] private Color progressBar2;

        public bool Purchased { get => purchased; set => purchased = value; }
        public int Price => price;
        public Color PlayerColor { get => playerColor; set => playerColor = value; }
        public Color EnemyColor { get => enemyColor; set => enemyColor = value; }
        public Color BGColor { get => bgColor; set => bgColor = value; }
        public Color ParticleColor { get => particleColor; set => particleColor = value; }
        public Color ProgressBarColor1 { get => progressBar1; set => progressBar1 = value; }
        public Color ProgressBarColor2 { get => progressBar2; set => progressBar2 = value; }
    }
}