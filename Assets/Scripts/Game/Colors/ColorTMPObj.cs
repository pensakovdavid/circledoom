﻿using UnityEngine;

namespace GameName.Scripts.Game.Colors {
    internal class ColorTMPObj : ColorObj {
        [SerializeField] private TMPro.TextMeshProUGUI textMeshProUGUI;
        
        void Start() => textMeshProUGUI.color = SetColor();
    }
}
