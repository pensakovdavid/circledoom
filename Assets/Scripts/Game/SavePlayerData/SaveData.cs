﻿using GameName.Scripts.Game.Colors;
using System.Collections.Generic;

namespace GameName.Scripts.Game.SavePlayerData {

    [System.Serializable()]
    public class SaveData {
        private int _curentCodeVersion;
        private int _money;
        private int _curentColor;

        private Dictionary<int, int> _results;
        private Dictionary<int, StarsBtnData> _stars;        

        public int CurentCodeVersion { get { return _curentCodeVersion;} set { _curentCodeVersion = value; } }
        public int Money { get { return _money;} set { _money = value; } }
        public int CurentColor { get { return _curentColor; } set { _curentColor = value; } }
        public Dictionary<int, int> Results { get { return _results; } set { _results = value; } }
        public Dictionary<int, StarsBtnData> Stars { get { return _stars; } set { _stars = value; } }
    }
}