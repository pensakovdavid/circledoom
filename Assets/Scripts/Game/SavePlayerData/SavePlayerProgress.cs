using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace GameName.Scripts.Game.SavePlayerData {
    public class SavePlayerProgress {
        private static SavePlayerProgress _instance;
        public static SavePlayerProgress Instance {
            get {
                if (_instance == null) {
                    _instance = new SavePlayerProgress();
                }
                return _instance;
            }
        }
        public void Save(SaveData save) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/CircleSave.dat");
            bf.Serialize(file, save);
            file.Close();
#if  UNITY_EDITOR
            Debug.Log("Game data saveded");
#endif
        }

        public SaveData Load() {
            if (File.Exists(Application.persistentDataPath
              + "/CircleSave.dat")) {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file =
                  File.Open(Application.persistentDataPath
                  + "/CircleSave.dat", FileMode.Open);
                SaveData data = (SaveData)bf.Deserialize(file);
                file.Close();

                if(data.CurentCodeVersion < PlayerAccount.Instance.CurentCodeVersion) {
                    data.CurentCodeVersion = PlayerAccount.Instance.CurentCodeVersion;
                    Save(data);
                }

#if UNITY_EDITOR
                Debug.Log("Game data loaded");
#endif
                return data;

            } else {
#if UNITY_EDITOR
                Debug.LogError("There is no save data");
#endif
                return null;
            }
        }
    }
}