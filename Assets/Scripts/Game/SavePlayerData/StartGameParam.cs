﻿using GameName.Scripts.Game.Colors;
using System.Collections.Generic;

namespace GameName.Scripts.Game.SavePlayerData {
    public static class StartGameParam {
        public static void GameParam(ref SaveData data) {
            data = new SaveData();
            data.Money = 200;
            data.CurentColor = 0;
            data.Results = new Dictionary<int, int>(8) {
                    {1, 0},
                    {2, 0},
                    {3, 0},
                    {4, 0},
                    {5, 0},
                    {6, 0},
                    {7, 0},
                    {8, 0},
                };
            data.Stars = new Dictionary<int, StarsBtnData>(8) {
                [1] = new StarsBtnData() { QuantityStars = 0, RemainedStars = 0, IsActive = false },
                [2] = new StarsBtnData() { QuantityStars = 0, RemainedStars = 0, IsActive = false },
                [3] = new StarsBtnData() { QuantityStars = 0, RemainedStars = 0, IsActive = false },
                [4] = new StarsBtnData() { QuantityStars = 0, RemainedStars = 0, IsActive = false },
                [5] = new StarsBtnData() { QuantityStars = 0, RemainedStars = 0, IsActive = false },
                [6] = new StarsBtnData() { QuantityStars = 0, RemainedStars = 0, IsActive = false },
                [7] = new StarsBtnData() { QuantityStars = 0, RemainedStars = 0, IsActive = false },
                [8] = new StarsBtnData() { QuantityStars = 0, RemainedStars = 0, IsActive = false },
            };
        }
    }
}
