﻿using GameName.Scripts.Game.Controllers;
using UnityEngine;

namespace GameName.Scripts.Game.SavePlayerData {
    public class PlayerAccount : MonoBehaviour {
        private static PlayerAccount _instance;
        public static PlayerAccount Instance {
            get {
                if (_instance == null) {
                    GameObject obj = new GameObject("PlayerAccount", typeof(PlayerAccount));
                    _instance = obj.GetComponent<PlayerAccount>();
                    _instance.Initialize();

                    DontDestroyOnLoad(obj);
                }
                return _instance;
            }
        }
        #region data
        private SaveData _data;
        private int _curentCodeVersion = 3;
        #endregion
        public SaveData Data => _data;
        public int CurentCodeVersion => _curentCodeVersion;

        public void SaveChanges() {
            _data.CurentColor = ColorController.Instance.CurentColor;
            _data.Results = ScoreController.Instance.BestScoreLvl;
            _data.Money = MoneyController.Instance.Money;
            _data.Stars = StarsController.Instance.BtnStarsData;
            SavePlayerProgress.Instance.Save(_data);
        }

        void Initialize() {
            EventsController.Instance.OnEndGame += EndGame;
            FirstStart(ref _data);

            _data = SavePlayerProgress.Instance.Load();
        }

        void EndGame() => SaveChanges();

        void FirstStart(ref SaveData data) {
            int _isFirst = PlayerPrefs.GetInt("IsFirst");
            if (_isFirst == 0) {
                StartGameParam.GameParam(ref data);
                SavePlayerProgress.Instance.Save(_data);
#if UNITY_EDITOR
                Debug.Log("first run");
#endif
                PlayerPrefs.SetInt("IsFirst", 1);
            }
#if UNITY_EDITOR
            else
                Debug.Log("welcome again!");
#endif
        }
    }
}