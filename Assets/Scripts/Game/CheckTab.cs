using GameName.Scripts.Game.Controllers;
using UnityEngine;

namespace GameName.Scripts.Game {
    public class CheckTab : MonoBehaviour {
        // TODO fix first tab
        private bool _firstTab = false;

        private void OnMouseDown() {
            EventsController.Instance.OnTabInvoke();
        }


        private void Update() {
            if (!_firstTab) {
                EventsController.Instance.OnTabInvoke();
                _firstTab = true;
            }


#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Space)) {
                EventsController.Instance.OnTabInvoke();
            }
#endif

        }
    }
}