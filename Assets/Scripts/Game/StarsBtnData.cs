﻿namespace GameName.Scripts.Game {
    [System.Serializable]
    public class StarsBtnData {
        private int _quantityStars;
        private bool _isActive;
        private int _remainedStars;

        public int QuantityStars {
            get { return _quantityStars; }
            set { _quantityStars = value; }
        }
        public int RemainedStars {
            get { return _remainedStars; }
            set { _remainedStars = value; }
        }
        public bool IsActive {
            get { return _isActive; }
            set { _isActive = value; }
        }
    }
}
