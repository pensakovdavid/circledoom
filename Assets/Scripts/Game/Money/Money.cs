using GameName.Scripts.Game.Controllers;
using UnityEngine;

namespace GameName.Scripts.Game.Money {
    public class Money : MonoBehaviour {
        [SerializeField] private GameObject particle;
        [SerializeField] private GameObject crystel;

        private bool _isCoinCollected = false;
        private float _timeToDestroy = 5f;
        private float _curentTime = 0;
        private int _quantityCoin = 1;
        public float TimeToDestroy { get => _timeToDestroy; set => _timeToDestroy = value; }
        public int QuantityCoins { get => _quantityCoin; set => _quantityCoin = value; }

        private void Update() {
            if (_curentTime >= _timeToDestroy && !_isCoinCollected) {
                Destroy(gameObject);
            }
            _curentTime += Time.deltaTime;
        }

        private void OnTriggerEnter2D(Collider2D collision) {
            if (collision.CompareTag("Player")) {
                GetComponent<Collider2D>().enabled = false;

                _isCoinCollected = true;

                if (crystel != null)
                    crystel.SetActive(false);
                if (particle != null)
                    particle.SetActive(true);

                MoneyController.Instance.AddMoney(_quantityCoin);
                Destroy(gameObject, 5f);
            }
        }
    }
}