using GameName.Scripts.Game.Controllers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameName.Scripts.Game.Money {
    public class MoneyGenerator : MonoBehaviour {
        [SerializeField] private GameObject moneyPrifab;
        [SerializeField] private float radiusSpawn;
        [SerializeField] private float timeToDestrot;

        [SerializeField, Range(1, 10)] private int moreCoins = 1;
        [SerializeField, Range(1, 10)] private int quantityCoins = 1;

        private int _conditionSpawn;
        private Vector2 _startPoint;
        private int QuantityScoreToSpawn {
            get {
                int a = 40 - 5 * SceneManager.GetActiveScene().buildIndex;
                if (a <= 5) a = 5;
                return a;
            }
        }

        void Start() {
            ScoreController.Instance.OnBoostScore += BoostScore;
            EventsController.Instance.OnEndGame += EndGame;            
        }

        private void EndGame() {
            ScoreController.Instance.OnBoostScore -= BoostScore;
            EventsController.Instance.OnEndGame -= EndGame;
        }

        void BoostScore(int i) {
            _conditionSpawn += 1;
            if (_conditionSpawn >= QuantityScoreToSpawn) {
                Spawn();
                _conditionSpawn = 0;
            }
        }

        void Spawn() {
            for (int i = 0; i < moreCoins; i++) {
                _startPoint = randOnCircle(radiusSpawn);
                moneyPrifab.transform.position = _startPoint;
                Money money = Instantiate(moneyPrifab).GetComponent<Money>();
                money.TimeToDestroy = timeToDestrot;
                money.QuantityCoins = quantityCoins;
            }
        }

        Vector2 randOnCircle(float radius) {
            float randAng = Random.Range(0, Mathf.PI * 2);
            return new Vector2(Mathf.Cos(randAng) * radius, Mathf.Sin(randAng) * radius);
        }
    }
}