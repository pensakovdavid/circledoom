﻿namespace GameName.Scripts.Game.Enums {
    enum DirectionMove {
        LeftToRight,
        RightToLeft,
        BottomToTop,
        TopToBottom
    }
}
