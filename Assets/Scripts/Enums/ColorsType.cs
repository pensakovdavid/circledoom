﻿namespace GameName.Scripts.Game.Enums {
    enum ColorType {
        PlayerColor,
        EnemyColor,
        ParticleColor,
        BGColor,
        ProgressBarColor1,
        ProgressBarColor2,
        Arbitrary
    }
}