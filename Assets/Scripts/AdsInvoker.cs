﻿using UnityEngine;
using GameName.Scripts.Game.Controllers;
using AppodealAds.Unity.Api;

namespace GameName.Scripts {
    internal class AdsInvoker : MonoBehaviour{
        private static AdsInvoker _instance;
        public static AdsInvoker Instance {
            get {
                if (_instance == null) {
                    GameObject obj = new GameObject("AdbInvoke", typeof(AdsInvoker));
                    _instance = obj.GetComponent<AdsInvoker>();
                    _instance.Initialize();
                    DontDestroyOnLoad(obj);
                }
                return _instance;
            }
        }

        private int _beforeShowingAds = 0;

        #region Data
        private const int _adsType = Appodeal.INTERSTITIAL;
        private const string _appKey = "c4b63c35d661eb1624c897e5fe6db80fc264b6714b52a2b0";
        private bool _processingPersonalData = false;
        #endregion

        public void Initialize() {
            _processingPersonalData = PlayerPrefs.GetInt("AgreePolicy") == 1 ? true : false;
            if (PlayerPrefs.GetInt("ads") != 1) {
                InitAppodeal();
            }
            if (PlayerPrefs.GetInt("ads") == 1) {
                DisablingADs();
                Debug.Log("ADs disamble!");
            }
        }

        void InitAppodeal() {
            EventsController.Instance.OnEndGame += EndGame;

            Appodeal.setChildDirectedTreatment(true);
            Appodeal.initialize(_appKey, _adsType, _processingPersonalData);
            Appodeal.updateConsent(_processingPersonalData);

            Appodeal.setTesting(true);
        }
      
        private void EndGame() {
            _beforeShowingAds += 1;
            if (_beforeShowingAds >= 5) {
                ShowAds();
                _beforeShowingAds = 0;
            }
        }

        void ShowAds() {
            if (Appodeal.isLoaded(_adsType)) {
                Appodeal.show(_adsType);
            }
        }

        public void DisablingADs() {
            EventsController.Instance.OnEndGame -= EndGame;
            Debug.Log("ADs are disabled!");
        }
    }
}