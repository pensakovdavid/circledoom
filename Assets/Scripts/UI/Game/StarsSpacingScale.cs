using GameName.Scripts.Game.Controllers;
using UnityEngine;
using UnityEngine.UI;

public class StarsSpacingScale : MonoBehaviour {
    [SerializeField] private GridLayoutGroup gridLayout;

    private bool _isEndGame = false;

    void Start() => EventsController.Instance.OnEndGame += () => _isEndGame = true;

    private void Update() {
        if (_isEndGame && gridLayout.spacing.x <= 140) {
            gridLayout.spacing = Vector2.Lerp(gridLayout.spacing, new Vector2(150f, 0f), Time.deltaTime);
        }
    }
}