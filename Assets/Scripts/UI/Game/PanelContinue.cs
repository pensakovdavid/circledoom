using UnityEngine;

namespace GameName.Scripts.UI.Game {
    internal class PanelContinue : MonoBehaviour {
        [SerializeField] private Animator animator;
        void Start() { if (animator != null) { animator.SetInteger("State", 1); } }
    }
}