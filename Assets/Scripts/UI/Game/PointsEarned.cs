using GameName.Scripts.Game.Controllers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameName.Scripts.UI.Game {
    internal class PointsEarned : MonoBehaviour {
        [SerializeField] private TMPro.TextMeshProUGUI curentScoreTMP;
        [SerializeField] private TMPro.TextMeshProUGUI bestScoreTMP;
        [SerializeField] private GameObject panelBestResult;

        private int _bestScore = 0;
        private int _score = 0;
        private int _curentLvl = 0;

        private void Start() {
            _curentLvl = SceneManager.GetActiveScene().buildIndex;
            EndGame();
        }

        void EndGame() {
            _score = ScoreController.Instance.Score;
            if (ScoreController.Instance.BestScoreLvl.ContainsKey(_curentLvl)) {
                _bestScore = ScoreController.Instance.BestScoreLvl[_curentLvl];
            } else
                _bestScore = 0;

            if (_bestScore <= 0) {
                panelBestResult.SetActive(false);
            }

            curentScoreTMP.text = _score.ToString();
            bestScoreTMP.text = _bestScore.ToString();            
        }
    }
}