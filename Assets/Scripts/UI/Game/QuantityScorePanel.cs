using GameName.Scripts.Game.Controllers;
using UnityEngine;

namespace GameName.Scripts.UI.Game {
    internal class QuantityScorePanel : MonoBehaviour {
        [SerializeField] private TMPro.TextMeshProUGUI _textMeshProUGUI;

        private int _score;
        private int _scoreScale;

        private void Start() {
            ScoreController.Instance.OnBoostScore -= ShowQuantityScore;
            ScoreController.Instance.OnBoostScore += ShowQuantityScore;
            _textMeshProUGUI.text = $"0 x1";
        }

        private void ShowQuantityScore(int obj) {
            _score = ScoreController.Instance.Score;
            _scoreScale = ScoreController.Instance.ScoreScale;

            _textMeshProUGUI.text = _score.ToString() + $" x{_scoreScale}";
        }
    }
}