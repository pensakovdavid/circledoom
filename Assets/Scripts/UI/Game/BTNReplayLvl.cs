using GameName.Scripts.Game;
using UnityEngine;

namespace GameName.Scripts.UI.Game {
    internal class BTNReplayLvl : MonoBehaviour {
        [SerializeField] private GameObject panel;
        public void OnClick() {
            SceneNavigator.ReloadingSceneAsync();
        }
    }
}