using GameName.Scripts.Game.Controllers;
using UnityEngine;
namespace GameName.Scripts.UI.Game {
    internal class QuantityMoneyPanel : MonoBehaviour {
        [SerializeField] private TMPro.TextMeshProUGUI _textMeshProUGUI;

        private int _moneyQuantity;

        private void Start() {
            MoneyController.Instance.OnBoostMoney -= ShowQuantityMoney;
            MoneyController.Instance.OnBoostMoney += ShowQuantityMoney;
            ShowQuantityMoney();
        }

        void ShowQuantityMoney() {
            _moneyQuantity = MoneyController.Instance.Money;
            _textMeshProUGUI.text = _moneyQuantity.ToString();
        }
    }
}