﻿using GameName.Scripts.Game;
using GameName.Scripts.Game.Controllers;
using UnityEngine;

namespace GameName.Scripts.UI.Game {
    internal class ShowCurentStars : MonoBehaviour {
        [SerializeField] private GameObject star1off;
        [SerializeField] private GameObject star2off;
        [SerializeField] private GameObject star3off;
        [SerializeField] private GameObject star1on;
        [SerializeField] private GameObject star2on;
        [SerializeField] private GameObject star3on;

        private StarsBtnData _starsBtnData;
        private int _quantityStars;
        private int _lvl;

        private void Start() {
            ScoreController.Instance.OnBoostScore += BoostScore;
            EventsController.Instance.OnEndGame += EndGame;
            BoostScore(0);
        }

        private void EndGame() {
            ScoreController.Instance.OnBoostScore -= BoostScore;
            EventsController.Instance.OnEndGame -= EndGame;
        }

        private void BoostScore(int obj) {
            _lvl = SceneNavigator.GetCurentScene();
            _starsBtnData = StarsController.Instance.BtnStarsData[_lvl];
            _quantityStars = _starsBtnData.QuantityStars;

            if (_quantityStars >= 1) {
                star1on.SetActive(true);
                star1off.SetActive(false);
            }
            if (_quantityStars >= 2) {
                star2on.SetActive(true);
                star2off.SetActive(false);
            }
            if (_quantityStars >= 3) {
                star3on.SetActive(true);
                star3off.SetActive(false);
            }

        }
    }
}
