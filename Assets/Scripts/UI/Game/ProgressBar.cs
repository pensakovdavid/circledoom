using GameName.Scripts.Game.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace GameName.Scripts.UI.Game {
    internal class ProgressBar : MonoBehaviour {
        [SerializeField] private Image progressBar;
        [SerializeField] private int actionFillBar = 0;
        [SerializeField] private float _speedFillingProgressBar = .05f;
        [SerializeField] private float _curentProgressBar = 0.001f;

        private bool _isProgressFill = false;

        private void Start() {
            ScoreController.Instance.OnBoostScore -= UpProgressBar;
            ScoreController.Instance.OnBoostScore += UpProgressBar;
        }


        private void Update() {
            progressBar.fillAmount = Mathf.Lerp(progressBar.fillAmount, _curentProgressBar, Time.deltaTime);

            if (progressBar.fillAmount >= 1f) {
                _isProgressFill = true;
                ScoreController.Instance.OnBoostScore -= UpProgressBar;
                EventsController.Instance.OnProgressBarFullInvoke(actionFillBar);
                progressBar.fillClockwise = false;
                _curentProgressBar = 0;
                _speedFillingProgressBar -= .005f;

            } else if (_isProgressFill && progressBar.fillAmount <= 0.001f) {
                ScoreController.Instance.OnBoostScore += UpProgressBar;
                progressBar.fillClockwise = true;
                _isProgressFill = false;
            }
        }

        public void UpProgressBar(int i) {
            _curentProgressBar += _speedFillingProgressBar * i;
        }
    }
}