﻿using GameName.Scripts.Game.Controllers;
using UnityEngine;

namespace GameName.Scripts.UI {
    internal class DisponceObjectEndGame : MonoBehaviour {
        [SerializeField] private GameObject[] objects;

        void Start() => EventsController.Instance.OnEndGame += EndGame;

        private void EndGame() {
            for (int i = 0; i < objects.Length; i++) {
                if (objects[i] != null)
                    objects[i].SetActive(false);
            }
            EventsController.Instance.OnEndGame -= EndGame;
        }
    }
}