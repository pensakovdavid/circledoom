using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(TMP_Text))]
public class OpenLinks : MonoBehaviour, IPointerClickHandler {
    public void OnPointerClick(PointerEventData eventData) {
        TMP_Text tmp_text = GetComponent<TMP_Text>();
        int link = TMP_TextUtilities.FindIntersectingLink(tmp_text, eventData.position, null);
        if (link != -1) {
            TMP_LinkInfo linkInfo = tmp_text.textInfo.linkInfo[link];
            Application.OpenURL(linkInfo.GetLinkID());
        }
    }
}
