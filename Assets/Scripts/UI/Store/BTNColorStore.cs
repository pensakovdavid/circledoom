using GameName.Scripts.Game.Colors;
using GameName.Scripts.Game.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace GameName.Scripts.UI.Store {

    public class BTNColorStore : MonoBehaviour {
        [SerializeField] private int btnIndex;
        [SerializeField] private Button btnColor;

        [SerializeField] private Image bgImage;
        [SerializeField] private Image enemyImage;
        [SerializeField] private Image playerImage;

        [SerializeField] private GameObject loockPanel;
        [SerializeField] private TMPro.TextMeshProUGUI price;

        private int _money;

        public int BtnIndex { get { return btnIndex; } set { btnIndex = value; } }

        private ColorElement _color;

        private void Start() {
            btnColor.onClick.AddListener(OnClick);

            SetParam();
        }

        void SetParam() {
            _color = ColorController.Instance.Colors.Colors[btnIndex];

            bgImage.color = _color.BGColor;
            enemyImage.color = _color.ProgressBarColor1;
            playerImage.color = _color.PlayerColor;

            loockPanel.SetActive(!_color.Purchased);
            price.text = _color.Price.ToString();
        }

        private void OnClick() {
            if (MoneyController.Instance.Money >= _color.Price && !_color.Purchased) {
                MoneyController.Instance.MakePurchase(_color.Price);
                _color.Purchased = true;
                ColorController.Instance.ChangeColor(btnIndex);

            } else if (_color.Purchased)
                ColorController.Instance.ChangeColor(btnIndex);
        }
    }
}