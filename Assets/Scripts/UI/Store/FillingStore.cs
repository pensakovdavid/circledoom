using GameName.Scripts.Game.Colors;
using GameName.Scripts.Game.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace GameName.Scripts.UI.Store {
    public class FillingStore : MonoBehaviour {
        [SerializeField] private Button storeBtnPref;
        [SerializeField] private Transform panelStore;

        private ColorsSO _colors;

        private void Start() {
            _colors = ColorController.Instance.Colors;

            for (int i = 0; i < _colors.Colors.Length; i++) {
                storeBtnPref.GetComponent<BTNColorStore>().BtnIndex = i;
                Instantiate(storeBtnPref, panelStore, false);
            }
        }
    }
}