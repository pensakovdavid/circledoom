using GameName.Scripts.Game.Controllers;
using UnityEngine;

namespace GameName.Scripts.UI.Menu {
    public class BestRecord : MonoBehaviour {
        [SerializeField] private TMPro.TextMeshProUGUI _textMeshProUGUI1;
        [SerializeField] private TMPro.TextMeshProUGUI _textMeshProUGUI2;
        [SerializeField] private GameObject panel;

        private int _lvl, _score;
        private string _text1, _text2;

        private void Start() {
            _lvl = ScoreController.Instance.GetBestRecord().Key - 1;
            _score = ScoreController.Instance.GetBestRecord().Value;
            if (_score != 0) {
                _text1 = $"Best score level {_lvl}";
                _text2 = _score.ToString();
            } else {
                panel.SetActive(false);
                _text1 = "";
                _text2 = "";
            }

            _textMeshProUGUI1.text = _text1;
            _textMeshProUGUI2.text = _text2;
        }
    }
}