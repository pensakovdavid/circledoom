using UnityEngine;
using UnityEngine.EventSystems;

namespace GameName.Scripts.UI.Menu {
    internal class PanelUpper : MonoBehaviour {
        [SerializeField] private GameObject panel;

        private Animator animator;
        private AnimationClip animationClip;
        private AnimationEvent evt;

        private void Start() {
            animator = panel.GetComponent<Animator>();

            evt = new AnimationEvent();
        }

        public void OnClickUp() {
            panel.SetActive(true);
            animator.SetInteger("State", 1);
        }
        public void OnClickDown() {
            animator.SetInteger("State", 2);
            Invoke("PanelActive", 1f);
        }       
        void PanelActive() {
            panel.SetActive(false);
        }
    }
}