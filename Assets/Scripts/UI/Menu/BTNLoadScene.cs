using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameName.Scripts.UI.Menu {
    internal class BTNLoadScene : MonoBehaviour {
        [SerializeField] private int lvl;

        public void OnClic() {
            if (lvl > SceneManager.sceneCountInBuildSettings)
                return;

            if (lvl == 0)
                SceneManager.LoadScene(1);
            else if (lvl >= 1)
                SceneManager.LoadSceneAsync(lvl+1);            
        }       
    }
}