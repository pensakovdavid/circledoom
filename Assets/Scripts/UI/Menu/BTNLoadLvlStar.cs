﻿using UnityEngine;
using GameName.Scripts.Game.Controllers;
using UnityEngine.UI;
using GameName.Scripts.Game;

namespace GameName.Scripts.UI.Menu{
    public class BTNLoadLvlStar : MonoBehaviour {
        [SerializeField] private bool active = false;
        [SerializeField] private int lvl;
        [SerializeField] private Button btn;

        [SerializeField] private GameObject lockPanel;
        [SerializeField] private TMPro.TextMeshProUGUI textMeshProUGUI;

        [SerializeField] private GameObject star1;
        [SerializeField] private GameObject star2;
        [SerializeField] private GameObject star3;

        private int _quantityStars;
        private int _totalStars;
        private StarsBtnData starsBtnData;

        private void Start() {
            SetParam();
            ShowBtn();
            ShowStars();
        }

        void SetParam() {
            int _lvl = SceneNavigator.GameLvl.lvls[lvl];
            btn.onClick.AddListener(OnClick);

            _totalStars = StarsController.Instance.GetTotalStars();
            starsBtnData = StarsController.Instance.BtnStarsData[_lvl];
            starsBtnData.RemainedStars = (_lvl - 2) * 3 - 1;

            if (_totalStars >= starsBtnData.RemainedStars) {
                starsBtnData.IsActive = true;
            }

            if (active) {
                starsBtnData.IsActive = active;
            }
        }

        void ShowBtn() {
            if (starsBtnData.IsActive) {
                lockPanel.SetActive(false);
                btn.interactable = true;
            } else if (!starsBtnData.IsActive) {
                textMeshProUGUI.text = (starsBtnData.RemainedStars - _totalStars).ToString();
                lockPanel.SetActive(true);
                btn.interactable = false;
            }
        }

        void ShowStars() {
            _quantityStars = starsBtnData.QuantityStars;
            if (_quantityStars >= 1) {
                star1.SetActive(true);
            }
            if (_quantityStars >= 2) {
                star2.SetActive(true);
            }
            if (_quantityStars >= 3) {
                star3.SetActive(true);
            }
        }

        void OnClick() {
            if (lvl > SceneNavigator.GameLvl.lvls.Length)
                return;
            SceneNavigator.LoadingSceneAsync(SceneNavigator.GameLvl.lvls[lvl]);
        }
    }
}
