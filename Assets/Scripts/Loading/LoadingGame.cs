using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameName.Scripts.Loading {
    public class LoadingGame : MonoBehaviour {

        [SerializeField] private GameObject privatePanel;
        [SerializeField] private Button btnYesPrivatePanel;
        [SerializeField] private Button btnNoPrivatePanel;
        [SerializeField] private Image progressBar;
        [SerializeField] private LoadingOperator[] loadingOperators;

        private float _progress = 0;
        private bool _isAgreePolicy = false;

        void Start() {
            btnYesPrivatePanel.onClick.AddListener(OnClickYes);
            btnNoPrivatePanel.onClick.AddListener(OnClickNo);

            progressBar.fillAmount = 0.1f;
        }
        void Update() {
            FerstStar();

            if (_isAgreePolicy) {
                for (int i = 0; i < loadingOperators.Length; i++) {
                    if (loadingOperators[i].isFinished)
                        continue;

                    loadingOperators[i].StartProcess();
                    _progress += (float)(1f / loadingOperators.Length);
                }
                progressBar.fillAmount = Mathf.Lerp(progressBar.fillAmount, _progress, .05f);

                if (progressBar.fillAmount >= 0.998) {
                    SceneManager.LoadScene(1);
                }
            }
        }

        void OnClickYes() {
            _isAgreePolicy = true;
            privatePanel.SetActive(false);
        }
        void OnClickNo() {
            Application.Quit();
        }

        void FerstStar() {
            int _isFirst = PlayerPrefs.GetInt("FirstPolicyLaunch");
            if (_isFirst == 0) {
                privatePanel.SetActive(true);
                if (_isAgreePolicy) {
                    PlayerPrefs.SetInt("FirstPolicyLaunch", 1);
                    PlayerPrefs.SetInt("AgreePolicy", 1);
                }
            } else
                OnClickYes();
        }
    }
}