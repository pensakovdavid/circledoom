using GameName.Scripts.Game.Colors;
using GameName.Scripts.Game.Controllers;
using UnityEngine;

namespace GameName.Scripts.Loading {
    internal class LoadingColorController : LoadingOperator {
        [SerializeField] private ColorsSO colorsSO;
        [SerializeField] private GameColorMaterial gameColorMaterial;

        public override bool isFinished { get; set; }

        public override void StartProcess() {
            ColorController.Instance.Initialize(colorsSO, gameColorMaterial);
            isFinished = true;
        }
    }
}