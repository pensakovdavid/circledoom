﻿using UnityEngine;

namespace GameName.Scripts.Loading {
    internal class LoadingFPSRate : LoadingOperator {
        public override bool isFinished { get; set; }
        public override void StartProcess() {
            Application.targetFrameRate = 60;
            isFinished = true;

        }
    }
}