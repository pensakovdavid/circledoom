﻿using GameName.Scripts.Game.Controllers;

namespace GameName.Scripts.Loading {
    internal class LoadingScoreController : LoadingOperator {

        public override bool isFinished { get; set; }
        public override void StartProcess() {
            _ = ScoreController.Instance;
            isFinished = true;
        }
    }
}
