﻿using GameName.Scripts.Game.Controllers;

namespace GameName.Scripts.Loading {
    internal class LoadingStarsController : LoadingOperator {

        public override bool isFinished { get; set; }
        public override void StartProcess() {
            _ = StarsController.Instance;
            isFinished = true;
        }
    }
}
