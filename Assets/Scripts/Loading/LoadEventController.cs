﻿using GameName.Scripts.Game.Controllers;

namespace GameName.Scripts.Loading {
    internal class LoadEventController : LoadingOperator {
        public override bool isFinished { get; set; }

        public override void StartProcess() {
            _ = EventsController.Instance;
            isFinished = true;
        }
    }
}