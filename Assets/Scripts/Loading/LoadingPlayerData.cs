using GameName.Scripts.Game.SavePlayerData;

namespace GameName.Scripts.Loading {
    internal class LoadingPlayerData : LoadingOperator {

        public override bool isFinished { get; set; }
        public override void StartProcess() {
            _ = PlayerAccount.Instance;
            isFinished = true;
        }
    }
}