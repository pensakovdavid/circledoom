using UnityEngine;

namespace GameName.Scripts.Loading {
    public abstract class LoadingOperator : MonoBehaviour {
        public abstract bool isFinished { get; set; }
        public abstract void StartProcess();
    }
}