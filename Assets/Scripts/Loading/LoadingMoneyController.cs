using GameName.Scripts.Game.Controllers;

namespace GameName.Scripts.Loading {
    internal class LoadingMoneyController : LoadingOperator {

        public override bool isFinished { get; set; }
        public override void StartProcess() {
            _ = MoneyController.Instance;
            isFinished = true;
        }
    }
}