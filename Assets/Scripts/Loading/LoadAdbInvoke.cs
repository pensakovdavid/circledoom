﻿namespace GameName.Scripts.Loading {
    internal class LoadAdbInvoke : LoadingOperator {
        public override bool isFinished { get; set; }

        public override void StartProcess() {
            _ = AdsInvoker.Instance;            
            isFinished = true;
        }
    }
}