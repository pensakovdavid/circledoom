What is Izayoi Monospaced?
A retro-like monospaced font.

How log could you made this font?
Several days.

Do you have alternates?
Yes. There are three of them, which can be accessed via Stylistic alternate feature or the glyph palette(when you have an application that supports OpenType features).

Do you have kern pairs?
No. Like lots of monospaced fonts, there are no kern pairs.

Do you have ligatures?
No. I don't make ligatures anymore because I want to have all the glyphs to be made in a same width.

Do you have languages supported?
Yes. There are known languages that use the Latin alphabet.

Now what license is it?
Freeware. I'm happy to see what's using this font in here.

Who is she?
Sakuya Izayoi, yeah. I named this font after her.

What game where did she debuted?
The Embodiment of Scarlet Devil! That's cool!

What happened to the kanji?
Fewer fonts doesn't render kanji very well, so I encoded at Macintosh Latin instead. To type 東方紅魔郷, use the characters in order:
† = 東
‡ = 方
≈ = 紅
≤ = 魔
≥ = 郷

How do I romanize the kanji?
It should be "Tōhō Kōmakyō", but also be "Toho Komakyo", or the not-much-used wāpuro romanization would being "Touhou Koumakyou"

What does it mean?
It means Eastern Lands of the Scarlet Devil in Japanese!

What kind of display where it can be used for?
It's perfect for headlines, coding and video subtitles like some other fonts(for e.g. anime shows with the original Japanese audio).